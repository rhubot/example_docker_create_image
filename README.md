# Setup

For this example, you'll need to clone [install_scripts](https://bitbucket.org/rhubot/install_scripts) and [create_scripts](https://bitbucket.org/rhubot/create_scripts) and have configured both
```
git clone https://bitbucket.org/rhubot/install_scripts.git
git clone https://bitbucket.org/rhubot/create_scripts.git
cd install_scripts
./configure_install_scripts.sh
cd ../create_scripts
./configure_create_scripts.sh
cd ..
source ~/.bashrc
```

and of course you will want to clone this repo
```
git clone https://bitbucket.org/rhubot/example_docker_create_image.git
```

If you don't have Docker installed already, install that now.
```
${INSTALL_SCRIPTS_DIR}/install_docker.sh
```

# Compile/Run
To create the Docker image and run it, you can use the provided script
```
cd ./example_docker_create_image
./make_docker_image.sh
```
The first half of the output will be the log of the build process.
The second half of the output is expected to be:
```
.
|-- test1.txt
|-- test2.txt
|-- test3
|   |-- test4.txt
|   `-- test5.txt
`-- test6
    `-- test7.txt
```

You can run the image again (which will give you just the expected output above) using the following command:
```
docker run -i test_tree_cmd
```