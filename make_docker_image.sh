#!/bin/bash -i

# Make a temp folder for files that Docker will have access to
mkdir -p temp

# Copy our docker file to the temp folder
cp ./Dockerfile ./temp/

cd temp

# I'm too lazy here to copy the specific install/create scripts that we need, so just copy them all
cp ${INSTALL_SCRIPTS_DIR}/*.sh ./
cp ${CREATE_SCRIPTS_DIR}/*.sh ./

# Copy the project setup script
#cp ${SETUP_SCRIPTS_DIR}/setup_project.sh ./

# Build the docker image
DOCKER_BUILDKIT=1 docker build --tag=test_tree_cmd .

# Cleanup the temp files
cd ..
rm -rf ./temp

# Run the docker image
docker run -i test_tree_cmd
