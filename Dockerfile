# Use base image ubuntu 16.04
FROM ubuntu:16.04

# Get a few things that Docker will need
RUN apt-get update && apt-get install -y sudo apt-utils

# Copy any script files files we need
COPY install_tree.sh /temp/
COPY colors.sh /temp/

# Change the working dir so Docker only knows about the files we care about
WORKDIR /temp/

# Run the install script
RUN export INSTALL_SCRIPTS_DIR=/temp && export CREATE_SCRIPTS_DIR=/temp && /temp/install_tree.sh

# Cleanup
RUN rm install_tree.sh
RUN rm colors.sh

# Make some test files so the "tree" command has something to print
WORKDIR /test_dir/

RUN touch test1.txt
RUN touch test2.txt
RUN mkdir -p test3
RUN touch ./test3/test4.txt
RUN touch ./test3/test5.txt
RUN mkdir -p test6
RUN touch ./test6/test7.txt

# Set the "tree" command to run when we run this image
CMD tree
#CMD cat install_tree.sh
